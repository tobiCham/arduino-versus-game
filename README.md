# Arduino-Versus-Game

A two player versus block shooter game, implemented on Arduino. Each player views the same blocks falling, and must dodge or shoot them

![](/demo/screen1.png)

More images / videos can be found in the [demos folder](/demo)