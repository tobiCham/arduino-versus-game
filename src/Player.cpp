#include <Arduino.h>
#include "../headers/Player.h"
#include "../headers/Projectile.h"
#include "../headers/Block.h"

//By default, have a size of 5s
Player::Player(Game *game, int maxHealth, int* leds) : Player(game, maxHealth, 5, leds) {}

Player::Player(Game *game, int maxHealth, int size, int* leds) : GameObject(game, 0, 0, size, 2), maxHealth(maxHealth) {
    //Set the startX so that the player is in the centre of the screen
    this->x = (game->getWidth() - size) / 2;
    this->projectile = new Projectile(game);
    this->health = maxHealth; //Start of with maxHealth

    //Initialise the LED array. There are the same number of LEDs as the max health. This is the dynamically
    //created array which will be stored, instead of using the array passed as a parameter
    this->leds = new int[maxHealth];

    //Iterate through each LED, set it to output and set put a HIGH voltage on the pin.
    //Then, copy the int value into the local array
    for(int i = 0; i < maxHealth; i++) {
        int pin = leds[i];
        pinMode(pin, OUTPUT);
        digitalWrite(pin, HIGH);
        this->leds[i] = pin;
    }
}
Player::~Player() {
    delete projectile;
    delete leds;
}

void Player::update() {
    //Gets the current value from the potentiometer
    //Limit its value to between certain values, and scale it for movement
    int value = analogRead(A0);
    value -= 300;
    if(value < 0) value = 0;
    if(value > 440) value = 440;

    //Move the player to a position based on the read value from the pin
    updatePosition(value / 40);
}

void Player::onIntersect(GameObject *otherObject) {
    //The only object which the player can intersect with is a Block
    if(otherObject->getType() == GameObjectType::BLOCK) {
        Block* block = (Block*) otherObject;
        if(!block->isValid()) return; //If the block isn't valid, ignore it

        //Damage the player, and invalidate the block
        damage(block);
        block->setValid(false);
    }
}


void Player::shootProjectile() {
    //Only fire if the projectile isn't on the screen
    if(projectile->isValid()) return;
    projectile->fire();
}

void Player::damage(Block *block) {
    //Decrease health
    health--;
    updateHealth();

    //The player has lost. Write to the Serial port that this has occured, then set to the lost state
    if(health <= 0) {
        Serial.write(GameNetworker::LOSE);
        game->setState(GameState::LOST);
    }
}

void Player::updateHealth() {
    //Sets each LED to High or Low to indicate the health of the player
    for(int i = 0; i < maxHealth; i++) {
        digitalWrite(leds[i], i < health ? HIGH : LOW);
    }
}

void Player::updatePosition(int position) {
    //Sets the x position, bounding it to be on the screen
    if(position < 0) position = 0;
    if(position + getWidth() >= game->getWidth()) position = game->getWidth() - getWidth();
    x = position;
}
