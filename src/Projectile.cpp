#include "../headers/Projectile.h"
#include "../headers/Block.h"
#include "../headers/Player.h"

Projectile::Projectile(Game* game) : GameObject(game, 0, 0, 1, 2) {
    hasHit = true; //Start off disabled
    speed = 2; //Default speed is 2 pixels per update
}

void Projectile::render(LCDRenderer* renderer) {
    //Only render the projectile if it is valid
    if(isValid()) GameObject::render(renderer);
}

void Projectile::update() {
    if(isValid()) {
        //Move the projectile speed pixels up the screen.
        moveY(speed);
    }
}

void Projectile::onIntersect(GameObject *otherObj) {
    //If the object the projectile hits is a block
    if(otherObj->getType() == GameObjectType::BLOCK) {
        Block* block = (Block*) otherObj;

        //Set this projectile to being hit, and invalidate the block so it is "destroyed"
        hasHit = true;
        block->setValid(false);
    }
}

void Projectile::fire() {
    hasHit = false; //Makes the projectile valid again

    Player* player = game->getPlayer();
    x = player->getX() + (player->getWidth() / 2); //Set x coordinate to the centre of the player
    y = player->getY() + 1; //Start at the next pixel up from the player
}

bool Projectile::isValid() {
    //Has to be visible on the screen and not hit any blocks
    return !hasHit && y + getHeight() < getGame()->getHeight();
}