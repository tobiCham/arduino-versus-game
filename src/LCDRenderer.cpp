#include "../headers/LCDRenderer.h"

//Default constructor for a default LCD display made up of 16x2 blocks of 5x8 pixels, with 8 custom characters
LCDRenderer::LCDRenderer(LiquidCrystal *lcd) : LCDRenderer(lcd, 8, 5, 2, 16, 8) {}

LCDRenderer::LCDRenderer(LiquidCrystal *lcd, int cellWidth, int cellHeight, int cellsX, int cellsY, int maxCustomChars)
        : cellWidth(cellWidth), cellHeight(cellHeight), cellsX(cellsX), cellsY(cellsY), maxCustomChars(maxCustomChars),
          width(cellWidth * cellsX), height(cellHeight * cellsY),
          pixelsLen(cellsX * cellsY * cellWidth), customCharsLen(maxCustomChars * cellWidth) {

    this->lcd = lcd;
    this->pixels = new byte[pixelsLen];
    this->customChars = new byte[customCharsLen];

    //Initilise the pixels array to be 0
    for (int i = 0; i < pixelsLen; i++) pixels[i] = 0;
}

LCDRenderer::~LCDRenderer() {
    delete pixels;
    delete customChars;
}

void LCDRenderer::fillRect(int x, int y, int width, int height) {
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            //Sets the pixels at each coordinate in the rectangle to be on
            setPixel(x + i, y + j, true);
        }
    }
}

void LCDRenderer::setPixel(int x, int y, bool on) {
    //Return if the coordinate is out of bounds
    if (x < 0 || y < 0 || x >= width || y >= height) return;

    //Gets the index for cell row from the pixel coordinates
    int index = getPixelIndex(x / cellWidth, y / cellHeight, x % cellWidth);

    //Each cell row is a byte, with each column being one bit. E.g. the byte may be:
    //01000. The 1 here indicates that the pixel should be on.
    //The mask is a value to combine with the current data using some binary operation.
    //It starts as a 1, which is shifted to the left based on the coordinate specified.
    //E.g. to set the 3rd pixel in the example above, the mask would be 00100.
    int mask = 1 << cellHeight - (y % cellHeight) - 1;

    //Combine the mask with the current value. If the pixel is off, the mask is inverted and an AND operation is performed
    //instead. Examples:
    //On: 11000 OR 00010 = 11010
    //Off: 11110 AND 11101 = 11100
    if (on) pixels[index] |= mask;
    else pixels[index] &= ~mask;
}

void LCDRenderer::render() {
    //Clear the LCD and reset the customChars array + data
    lcd->clear();
    for (int i = 0; i < customCharsLen; i++) customChars[i] = 0;
    customCharCounter = 0;

    renderCells();

    //After rendering, clear the pixels array
    for (int i = 0; i < pixelsLen; i++) pixels[i] = 0;
}

void LCDRenderer::renderCells() {
    //Iterates through each cell.
    for (int i = 0; i < cellsX; i++) {
        for (int j = 0; j < cellsY; j++) {
            //Attempts to render a cell if the
            if (shouldRender(i, j)) {
                renderCell(i, j);
            }
        }
    }
}

bool LCDRenderer::renderCell(int cellX, int cellY) {
    //Gets or creates a custom character for the cell
    short id = createCharacter(cellX, cellY);

    //An ID of less than 0 will indicate no custom characters are available
    if (id < 0) return false;

    //Otherwise, write the custom character ID onto the screen
    lcd->setCursor(cellY, cellX);
    lcd->write((byte) id);
    return true;
}

short LCDRenderer::createCharacter(int cellX, int cellY) {
    byte *cell = getCell(cellX, cellY);

    //Start at -1. Iterate through each custom character, and check if the cell and the character match
    //If they do, set the cellID to the index.
    short cellID = -1;
    for (short i = 0; i < customCharCounter; i++) {
        if (charCellMatch(cell, i)) {
            cellID = i;
            break;
        }
    }
    //If the cell ID isn't -1, then it is valid, return it
    if (cellID >= 0) return cellID;

    //From here, a custom character must be produced. If no more custom characters are allowed, return -1
    if (customCharCounter >= maxCustomChars) return -1;

    //Create the character on the LCD
    lcd->createChar((byte) customCharCounter, cell);

    //Add the custom character to the array
    for (int i = 0; i < cellWidth; i++) {
        customChars[getCustomCharIndex(customCharCounter, i)] = cell[i];
    }
    customCharCounter++;
    return customCharCounter - 1;
}

bool LCDRenderer::shouldRender(int x, int y) {
    //Iterates through each cell row and checks if its empty
    for (int i = 0; i < cellWidth; i++) {
        if (pixels[getPixelIndex(x, y, i)] != 0) {
            return true;
        }
    }
    return false;
}

bool LCDRenderer::charCellMatch(byte *cell, int charIndex) {
    //Iterates through each row in the cell, and check if it matches a specific cell row
    for (int i = 0; i < cellWidth; i++) {
        if (cell[i] != customChars[getCustomCharIndex(charIndex, i)]) return false;
    }
    return true;
}
