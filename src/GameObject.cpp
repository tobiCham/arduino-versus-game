#include "../headers/GameObject.h"

GameObject::GameObject(Game *game) : GameObject(game, 0, 0, 1, 1) {}
GameObject::GameObject(Game *game, int x, int y, int width, int height) : game(game), x(x), y(y), width(width), height(height) {}

void GameObject::update() {} //Default update implementation does nothing

void GameObject::render(LCDRenderer *renderer) {
    //Fills a rectangle on the display of this object
    renderer->fillRect(getX(), getY(), getWidth(), getHeight());
}

bool GameObject::intersects(GameObject *obj) {
    //Checks if this object and the other object intersect
    return x + width > obj->x && y + height > obj->y && x < obj->x + obj->width && y < obj->y + obj->height;
}

void GameObject::moveY(int y) {
    //Checks for negative values
    bool negative = y < 0;
    if(y < 0) y = -y;

    //Step through each pixel the object moves
    for(int i = 0; i < y; i++) {
        checkCollisions(); //Check for collisions at each pixel

        //Move the object
        if(negative) this->y--;
        else this->y++;
    }
    //Check collisions again after moving
    checkCollisions();
}

void GameObject::checkCollisions() {
    GameObject** objects = game->getObjects();
    int total = game->getTotalObjects();

    //Iterate through each GameObject in the game
    for(int i = 0; i < total; i++) {
        GameObject* object = objects[i];
        if(object == this) continue; //Ignore if the object is this object

        //If the object and this object intersect. Call both methods in case one method has a different implementation
        if(intersects(object) && object->intersects(this)) {
            //Call the onIntersect method on both objects
            onIntersect(object);
            object->onIntersect(this);
        }
    }
}
