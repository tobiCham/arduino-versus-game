#include <Arduino.h>
#include <Math.h>
#include "../headers/Game.h"
#include "../headers/Block.h"
#include "../headers/Player.h"
#include "../headers/Projectile.h"

Game::Game(LCDRenderer* renderer, GameNetworker* networker, bool controller, int maxBlocks) :
        renderer(renderer), networker(networker), controller(controller), maxBlocks(maxBlocks) {

    blocks = new Block*[maxBlocks]; //Create an array of all the Blocks
    this->gameState = GameState::PAUSED; //Default state to be paused

    //Objects are the player, their projectile, and the blocks.
    this->totalObjects = maxBlocks + 2;
    this->objects = new GameObject*[totalObjects];
}

//Destructor. Clean up all objects used in memory
Game::~Game() {
    delete player;

    for(int i = 0; i < maxBlocks; i++) delete blocks[i];
    delete blocks;
    delete objects;
}

void Game::init(int* playerLEDs, int ledCount) {
    player = new Player(this, ledCount, playerLEDs);

    //1st Object is the player, 2nd object is the player's projectile
    objects[0] = player;
    objects[1] = player->getProjectile();

    //Create each block. Insert it into the blocks and objects arrays
    for(int i = 0; i < maxBlocks; i++) {
        Block* block = new Block(this);
        blocks[i] = block;
        objects[i + 2] = block;
    }

    blockTimer = 8; //Two seconds until first block
}

void Game::update() {
    //Only update if either the player has won, or the game is active
    if(!(gameState == GameState::ACTIVE || gameState == GameState::WON)) return;

    if(gameState == GameState::WON) {
        //Every 2 ticks (0.5 seconds), toggle the winState, and also toggle the state of the player's LEDs
        winTimer--;
        if(winTimer == 0) {
            winTimer = 2;
            winState = !winState;

            for(int i = 0; i < player->getMaxHealth(); i++) {
                digitalWrite(player->getLEDs()[i], winState ? HIGH : LOW);
            }
        }
        return;
    }

    //Check with the networker if the other player has lost the game. If so, set the state to won
    if(networker->hasOtherPlayerLost()) {
        setState(GameState::WON);
        return;
    }

    Block::setSpeed(Block::getSpeed() + 0.0025f); //Increase the speed of all blocks

    //Update each game object
    for(int i = 0; i < getTotalObjects(); i++) objects[i]->update();

    //A timer value <= 0 indicates a new block should spawn
    blockTimer--;
    if(blockTimer <= 0) {
        //Iterate through the array and attempt to find a block which is invalid to initialise
        for(int i = 0; i < maxBlocks; i++) {
            Block* block = blocks[i];
            if(block->isValid()) continue;

            //Place a block at a random position on the top of the screen. Restrict to either
            //column on the display to reduce the nubmer of custom characters required
            int xPos = random(14);
            if(xPos >= 7) xPos += 1;
            block->init(xPos, getHeight() - 2, 2, 2);
            newBlockTime(); //Restart the block timer
            break;
        }
    }

    //Render each game object
    for(int i = 0; i < getTotalObjects(); i++) objects[i]->render(renderer);

    //If at the end of the update, the game is still active, then apply the changes to the LCD display
    if(gameState == GameState::ACTIVE) {
        renderer->render();
    }
}

void Game::newBlockTime() {
    //Generates a block time based on the speed of the blocks to ensure a maximum of 5 blocks on the screen
    //at any time. Use the ceiling function to round up if the result is not an integer.
    blockTimer = (int) ceil((float) getHeight() / (maxBlocks * Block::getSpeed()));
}

void Game::setState(GameState state) {
    this->gameState = state;

    //The only states which cause any changes are the WON and LOST states
    if(!(state == GameState::WON || state == GameState::LOST)) return;

    LiquidCrystal* lcd = renderer->getLCD();
    lcd->clear();

    //Turn off all the player's LEDs. If the player has won, these LEDs will flash
    for(int i = 0; i < player->getMaxHealth(); i++) {
        digitalWrite(player->getLEDs()[i], LOW);
    }

    //Display a message to the user based on the state
    if(state == GameState::WON) {
        lcd->write("You won!");

        //Intialise the variables to cause the LEDs to flash
        winTimer = 2;
        winState = false;
    }
    if(state == GameState::LOST) {
        lcd->write("You lost.");
    }
}
