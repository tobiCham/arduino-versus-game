#include <Arduino.h>
#include "../../headers/network/ControllerNetworker.h"

ControllerNetworker::ControllerNetworker(LiquidCrystal* display, int ledCount, int* leds) : GameNetworker(display, ledCount, leds) {
    this->p2Ready = false;

    //To generate a random seed, add onto a total the result of an analogRead
    //of each Digital and Analog pin 50 times. Should be random.
    int total = 0;
    for(int i = 0; i < 50; i++) {
        for(int j = 0; j <= 5; j++) total += analogRead(A0 + j);
        for(int j = 0; j <= 13; j++) total += analogRead(j);
    }
    //The seed which is sent must be 1 byte - remainder it with 256
    this->seed = (int) ((unsigned int) total % 256);
}

void ControllerNetworker::tryConnect() {
    while(true) {
        //All players are ready
        if(ready && p2Ready) {
            //Send to the Client the game is starting, and send the seed which will be used
            //to generate blocks
            Serial.write(STARTING);
            Serial.write(this->seed);
            return;
        }

        if(!connected) {
            checkConnection();
            continue;
        }

        //Always connected from here
        if(buttonUpdate && !ready) {
            buttonUpdate = false;
            setReady();
        }

        if(!p2Ready) {
            //If the second player isn't ready, read bytes until no more available. If the byte indicates that
            //the player is ready, then set the player to be ready
            while(Serial.available() > 0) {
                if(Serial.read() == CLIENT_READY) {
                    p2Ready = true;
                    break;
                }
            }
        }
    }
}

void ControllerNetworker::checkConnection() {
    if(connected) return;

    //Write out on Serial that a connection is ready to be started
    Serial.write(BEGIN_CONNECTION);

    //Read bytes from the Serial connection as long as there are more to read.
    //If the byte is the BEGIN_CONNECTION byte, the Arduinos should now be connected.
    while(Serial.available() > 0) {
        if(Serial.read() == BEGIN_CONNECTION) {
            connected = true;
            setWaitReady();
            return;
        }
    }
    delay(1000); //Sleep for 1 second, to only ping every second
}