#include <Arduino.h>
#include "../../headers/network/ClientNetworker.h"

ClientNetworker::ClientNetworker(LiquidCrystal* display, int ledCount, int* leds) : GameNetworker(display, ledCount, leds) {
    this->seed = -1; //Initialise the seed to -1. Will be positive after connecting
}

void ClientNetworker::tryConnect() {
    //Constantly loop until the Arduinos have successfully connected and both players are ready
    while(true) {
        //Attempt to connect if not connected
        if(!connected) {
            checkConnection();
            continue;
        }

        //Handle the button update. Sets to being ready if not already indicated
        if(buttonUpdate && !ready) {
            buttonUpdate = false;
            setReady();
        }

        if(connected && ready) {
            //If the Arduinos are connected and the player is ready, wait on the Controller to send data
            //indicating the game should start
            while(Serial.available() > 0) {
                if(Serial.read() == STARTING) {
                    //Wait for the next piece of data to be sent (the seed)
                    while(Serial.available() <= 0) {}
                    this->seed = Serial.read();
                    return;
                }
            }
        }
    }
}

void ClientNetworker::checkConnection() {
    //Read bytes from the Serial connection as long as there are more to read.
    //If the byte is the BEGIN_CONNECTION byte, the Arduinos should now be connected.
    while(Serial.available() > 0) {
        if(Serial.read() == BEGIN_CONNECTION) {
            //Echo the data back to the Controller and notify the user the button can now be pressed
            Serial.write(BEGIN_CONNECTION);
            connected = true;
            setWaitReady();
            return;
        }
    }
    //Here, no connection is available. Delay for 1 second until the next connection check
    delay(1000);
}

void ClientNetworker::setReady() {
    //Call the super method, then write out to Serial to the other Arduino to indicate that this Arduino is ready
    GameNetworker::setReady();
    Serial.write(CLIENT_READY);
}