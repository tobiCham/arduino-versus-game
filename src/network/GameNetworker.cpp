#include <Arduino.h>
#include "../../headers/network/GameNetworker.h"

GameNetworker::GameNetworker(LiquidCrystal *display, int ledCount, int* leds) : display(display), ledCount(ledCount), leds(leds) {
    this->ready = false;
    this->connected = false;
    this->buttonUpdate = false;

    Serial.begin(9600);

    //Sets up the LCD display to show the user they are waiting for a connection
    display->clear();
    display->write("Waiting for");
    display->setCursor(0, 1);
    display->write("Connection");
}

bool GameNetworker::hasOtherPlayerLost() {
    //Read bytes from the Serial connection as long as there are more to read. If the byte is the LOSE byte,
    //then return true
    while(Serial.available() > 0) {
        if(Serial.read() == LOSE) return true;
    }
    return false;
}

void GameNetworker::readyButtonPress() {
    buttonUpdate = true;
}

void GameNetworker::setReady() {
    //Updates the LCD, and sets each LED to on
    display->clear();
    display->write("Ready");

    for(int i = 0; i < ledCount; i++) digitalWrite(leds[i], HIGH);
    ready = true;
}

void GameNetworker::setWaitReady() {
    //Updates the LCD the tell the user to press the button
    display->clear();
    display->write("Press Button");
    display->setCursor(0, 1);
    display->write("When Ready");
    ready = false;
}