#include "../headers/Block.h"
#include "../headers/Player.h"
#include "../headers/Projectile.h"

float Block::speed = 1; //Initial the default speed to be 1

Block::Block(Game* game) : GameObject(game) {
    this->valid = false; //Initialise valid to false initially
}

void Block::init(int x, int y, int width, int height) {
    this->x = x;
    this->y = y;
    this->accurateY = y;
    this->width = width;
    this->height = height;
    valid = true;
}

void Block::update() {
    if(!valid) return; //Only update if the Block is valid

    //As the move method only takes in an integer value to move, so only move an
    //integer speed, while maintaining an accurate y location as a float
    float beforeY = accurateY;
    y = (int) accurateY;

    accurateY -= speed;
    moveY((int) -speed);

    //If has moved down off the bottom of the screen, set invalid
    if(y + height <= 0) {
        valid = false;
    }
}

void Block::render(LCDRenderer* renderer) {
    //Only render if valid
    if(!valid || y < 0 || y + height > renderer->getHeight()) return;
    renderer->fillRect(x, y, width, height);
}

bool Block::intersects(GameObject* otherObject) {
    //Can never intersect if not valid
    if(!isValid()) return false;
    return GameObject::intersects(otherObject);
}

bool Block::isValid() {
    return valid && y >= 0 && y + height < game->getRenderer()->getHeight();
}