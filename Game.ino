#include <Arduino.h>

#include "headers/LCDRenderer.h"
#include "headers/Block.h"
#include "headers/Player.h"
#include "headers/network/ControllerNetworker.h"
#include "headers/network/ClientNetworker.h"

//Number of LEDs
#define LED_COUNT 5

//True for this Arduino being the Controller, false to be the Client.
//One Arduino will have this as true, the other as false
#define CONTROLLER true

//Initialise the LCD
LiquidCrystal lcd(3, 4, 5, 6, 7, 8);

//Declare the Renderer, Game and Networker variables
LCDRenderer render(&lcd);
Game* game;
GameNetworker* networker;

int playerLEDs[LED_COUNT] {9, 10, 11, 12, 13};

bool isConnected = false; //Whether the Arduino has connected to another Arduino

volatile bool buttonUpdate = false; //Flag for whether a button press needs to be handled
int startTimer = -1; //Timer to count down to the start of a game

//Declare functions
void buttonPress();
void startGame();

void setup() {
    lcd.begin(16, 2);
    attachInterrupt(digitalPinToInterrupt(2), buttonPress, FALLING);

    for(int i = 0; i < 5; i++) pinMode(playerLEDs[i], OUTPUT);

    //Initialise the networker to either be the ControllerNetworker or ClientNetworker
    if(CONTROLLER) networker = new ControllerNetworker(&lcd, 5, playerLEDs);
    else networker = new ClientNetworker(&lcd, 5, playerLEDs);

    //Wait for the networker to connect to the other Arduino
    networker->tryConnect();

    //After connected, start the timer at 5 seconds to count down
    startTimer = 5;

    //Initialise the seed for the random generator to the seed from the networker.
    //This will either be generated or recieved.
    randomSeed(networker->getSeed());
    isConnected = true;
}

void loop() {
    //If there is a game currently active
    if(game != nullptr) {
        //Handle the button update
        if(buttonUpdate) {
            buttonUpdate = false;
            game->getPlayer()->shootProjectile();
        }

        //Update the button, then delay for 250ms for a refresh rate of 4Hz
        game->update();
        delay(250);
        return;
    }

    //Otherwise here, there is no game. If the start timer is more than 0, then display the count down
    if(startTimer > 0) {
        lcd.clear();
        lcd.write("Starting in");
        lcd.setCursor(0, 1);
        lcd.write('0' + startTimer);

        delay(1000);
        startTimer--;

        //When the timer hits 0, start the game
        if(startTimer == 0) startGame();
    }
}

void startGame() {
    //Reset the buttonUpdate. Have had a bug where the button update was not handled from the networker and so
    //the projectile was fired when the game started
    buttonUpdate = false;

    //Use a maximum of 5 blocks. 8 custom characters allowed, two for the player, 1 for the projectile, leaving
    //5 available to be used for blocks.
    game = new Game(&render, networker, CONTROLLER, 5);
    game->init(playerLEDs, LED_COUNT);
    game->setState(GameState::ACTIVE);
}

void buttonPress() {
    //Forward the button press to the networker if need be, otherwise set the flag
    if(!isConnected) networker->readyButtonPress();
    else buttonUpdate = true;
}
