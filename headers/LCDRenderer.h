#ifndef ARD_LCD_RENDERER_H
#define ARD_LCD_RENDERER_H

#include <LiquidCrystal.h>

//A byte is an unsigned value of 8 bits (0 - 255)
typedef uint8_t byte;

/**
 * Class which manages rendering to the LCD display using a Graphics library type approach
 *
 * Arduino LCD displays are displays which are designed to simply display text, and does not support direct
 * modification of pixels, which would be needed for graphics.
 * However, the LCD does support up to 8 different custom characters which can be rendered accross the display.
 * By utilising these custom characters, graphics can be displayed onto the display. The limitation here is in
 * the amount which can be rendered at the same time.
 */
class LCDRenderer {
public:
	LCDRenderer(LiquidCrystal *lcd, int cellWidth, int cellHeight, int cellsX, int cellsY, int maxCustomChars);
	LCDRenderer(LiquidCrystal *lcd);
	~LCDRenderer();

    /**
     * Applies the changes to the LCD. Will clear the display first
     * Be aware, that not all render operations may appear on the display due to the limitations
     * described in the class declaration
     */
    void render();

    /**
     * Fills in a rectangle on the screen
     */
	void fillRect(int x, int y, int width, int height);

    /**
     * Sets the value of a pixel directly on the screen
     * @param on True for the pixel being on, false for it being off
     */
	void setPixel(int x, int y, bool on);

    //Getter methods for the variables
	inline LiquidCrystal *getLCD() { return lcd; }
	inline int getWidth() { return width; }
    inline int getHeight() { return height; }
    inline int getMaxCustomChars() { return maxCustomChars; }

private:
    LiquidCrystal* lcd; //Pointer to the LiquidCrystal library in Arduino

    const int width; //Width of the screen in pixels
    const int height; //Height of the screen in pixels
    const int cellWidth; //The width of each cell in pixels
    const int cellHeight; //The height of each cell in pixels
    const int cellsX; //Number of cells in the x direction on the screen
    const int cellsY; //Number of cells in the y direction on the screen

    const short pixelsLen; //Length of the pixels array

    //A 1D array of bytes, with each byte containing a row of pixels in a cell.
    //Acts as a 3D array to increase performance.
    //The third dimension of the array is effectively an array of data for one cell
    byte *pixels;

	const int maxCustomChars; //Maximum number of custom characters allowed
    const int customCharsLen; //Length of the customChars array
    short customCharCounter; //The effective size of the customChars array. Increases as characters are added

    //A 1D array of bytes, with each byte representing a row of a custom character.
    //Acts as a 2D array to increase performance. Each "sub array" represents a cell (custom character)
    byte* customChars;

    /**
     * Renders each cell on the display
     */
    void renderCells();

    /**
     * Both parameters are in cell coordinates, not pixels.
     * Effectively checks if the cell has any data in
     * @return Whether the cell at a particular x-y coordinate should be renderer.
     */
	bool shouldRender(int x, int y);

    /**
     * @return Whether a custom character is the same as a particular cell
     */
    bool charCellMatch(byte* cell, int charIndex);

    /**
     * Both parameters are in cell coordinates, not pixels.
     * @return The custom character index which will represent the specified cell, or -1 if out of characters
     */
    short createCharacter(int cellX, int cellY);

    /**
     * Renders a particular cell onto the LCD
     * @return Whether or not the render was successful. Will only return false if no custom characters
     */
    bool renderCell(int cellX, int cellY);

    /**
     * @return The index in the pixels array for this cell row
     */
    inline int getPixelIndex(int x, int y, int row) {
        return (x * cellsY * cellWidth) + (y * cellWidth) + row;
    }
    /**
     * Both parameters are in cell coordinates, not pixels.
     * @return A byte array of the data of this cell.
     */
    inline byte *getCell(int x, int y) {
        return pixels + (x * cellsY * cellWidth) + (y * cellWidth);
    }
    /**
     * @return The index in the customChars array for a certain cell row
     */
		 inline int getCustomCharIndex(int customChar, int row) {
        return (customChar * cellWidth) + row;
    }
};

#endif
