#ifndef ARD_GAME_H
#define ARD_GAME_H
#include "LCDRenderer.h"
#include "network/GameNetworker.h"

//Forward declare classes to avoid circular includes
class Block;
class Player;
class GameObject;

/**
 * Enum containing the three different states the game can be in. Default will be ACTIVE
 */
enum GameState {
    PAUSED, ACTIVE, LOST, WON
};

class Game {

public:
    /**
     * @param isController True for this Arduino being the Controller, false for being the Client
     * @param maxBlocks Maximum number of blocks which can be on the screen at one time
     */
    Game(LCDRenderer* renderer, GameNetworker* networker, bool isController, int maxBlocks);
    ~Game();

    /**
     * Intialises the game, and all the GameObjects contained. Should be called after the constructor
     * and always before any other method
     */
    void init(int* playerLEDs, int ledCount);

    /**
     * Should be called each tick. Handles updates and rendering
     */
    void update();

    /**
     * @return The width of the screen, in pixels
     */
    inline int getWidth() { return renderer->getWidth(); }

    /**
     * @return The height of the screen, in pixels
     */
    inline int getHeight() { return renderer->getHeight(); }

    inline const int getMaxBlocks() { return maxBlocks; }

    /**
     * @return Number of game objects in the game
     */
    inline int getTotalObjects() { return totalObjects; }

    /**
     * @return Array of game objects in the game, with size equal to #getTotalObjects()
     */
    inline GameObject** getObjects() { return objects; }

    inline Block** getBlocks() { return blocks; }
    inline LCDRenderer* getRenderer() { return renderer; }
    inline GameNetworker* getNetworker() { return networker; }
    inline Player* getPlayer() { return player; }

    inline bool isController() { return controller; }
    inline bool isClient() { return !controller; }

    inline GameState getState() { return gameState; }

    /**
     * Sets a new state for the game. If WIN or LOSE, player LEDs and the LCD will also be updated
     */
    void setState(GameState state);

private:
    GameState gameState;

    const bool controller; //True if this Arduino is a Controller, false if its a Client

    int totalObjects; //Total number of objects on the screen
    GameObject** objects; //Array of GameObjects

    const int maxBlocks; //Total blocks allowed on the screen, also the blocks array size
    Block** blocks; //Array of Blocks

    int winTimer; //Acts as a timer to flash the LEDs if the player has won
    bool winState; //State of the LEDs if the player has won. True for on, false for off

    int blockTimer; //When the value hits 0, a new block will spawn

    LCDRenderer* renderer;
    GameNetworker* networker;
    Player* player;

    void newBlockTime(); //Sets the blockTimer to a new value based on the speed of blocks
};


#endif //ARD_GAME_H
