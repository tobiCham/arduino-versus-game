#ifndef ARD_PLAYER_H
#define ARD_PLAYER_H

#include "GameObject.h"

class Projectile;

class Player : public GameObject {
public:
    Player(Game *game, int maxHealth, int* leds);
    Player(Game *game, int maxHealth, int size, int* leds);
    ~Player();

    inline GameObjectType getType() override { return GameObjectType::PLAYER; };

    /**
     * Shoots the projectile from the player
     */
    void shootProjectile();

    /**
     * Called when a block hits a player, and so damages it
     * @param block The block which hit this player
     */
    void damage(Block* block);

    void update() override;

    void onIntersect(GameObject *otherObject) override;

    //Getter methods
    inline Projectile* getProjectile() { return projectile; }

    inline int getHealth() { return health; }
    inline int* getLEDs() { return leds; }
    inline const int getMaxHealth() { return maxHealth; }

private:
    Projectile* projectile; //Pointer to the projectile which the player can shoot
    int* leds; //Integer array of LED pins

    const int maxHealth;
    int health;

    /**
     * Updates the player's health, changing LEDs
     */
    void updateHealth();

    /**
     * Updates the player, ensuring doesn't go out of bounds
     * @param position x-coordinate
     */
    void updatePosition(int position);
};

#endif //ARD_PLAYER_H
