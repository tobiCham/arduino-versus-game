#ifndef ARD_GAMEOBJECT_H
#define ARD_GAMEOBJECT_H

#include <math.h>
#include "LCDRenderer.h"
#include "Game.h"

//An enum of all the different GameObject types. Used to distinguish between different GameObject objects
enum GameObjectType {
    PLAYER, PROJECTILE, BLOCK
};

class GameObject {

public:
    GameObject(Game* game, int x, int y, int width, int height);

    /**
     * Equivalent to GameObject(game, 0, 0, 1, 1);
     */
    GameObject(Game* game);

    /**
     * @return The type of this object. Pure virtual function - all sub classes must override.
     */
    virtual GameObjectType getType() = 0;

    /**
     * Renders this object to the display
     * @param renderer Renderer to render to the display.
     */
    virtual void render(LCDRenderer *renderer);

    /**
     * Updates the object. Movement should be done here
     */
    virtual void update();

    /**
     * @return Whether this object and the other object intersect
     */
    virtual bool intersects(GameObject* otherObject);

    /**
     * Event called when another object intersects with this object.
     * @param otherObject Object intersected with
     */
    virtual void onIntersect(GameObject* otherObject) {}

    //Getter methods for variables
    inline Game* const getGame() { return game; }
    virtual inline int getX() { return x; }
    virtual inline int getY() { return y; }
    virtual inline int getWidth() { return width; }
    virtual inline int getHeight() { return height; }

protected:
    Game* const game; //Constant pointer to the game. Cannot change
    int x, y, width, height;

    /**
     * Moves this object, checking at each pixel for intersection of other GameObjects
     * @param y Numbers of pixels to move in the y direction
     */
    void moveY(int y);

private:
    /**
     * Checks if any object intersects this object, and if so calls its intersect method
     */
    void checkCollisions();
};


#endif //ARD_GAMEOBJECT_H
