#ifndef ARD_CONTROLLER_NETWORKER_H
#define ARD_CONTROLLER_NETWORKER_H

#include "GameNetworker.h"

/**
 * This networker acts as the Controller/Master Arduino. It sends out an "are you there" piece of data
 * every second until it receives back data that there is a connection.
 * It waits on both players to indicate they are ready, then send back to the Client that the game is starting,
 * also sending the seed to use.
 */
class ControllerNetworker : public GameNetworker {

public:
    ControllerNetworker(LiquidCrystal* display, int ledCount, int* leds);

    void tryConnect() override;

private:
    bool p2Ready; //Whether the second player (Client Arduino) has indicated they are ready

    void checkConnection();
};


#endif //ARD_CONTROLLER_NETWORKER_H
