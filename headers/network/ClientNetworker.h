#ifndef ARD_CLIENT_NETWORKER_H
#define ARD_CLIENT_NETWORKER_H

#include "GameNetworker.h"

/**
 * This Networker acts as the Client/Slave. It waits for a connection from the Controller Arduino,
 * then echos back it is ready. When the button is pressed, the Controller is notified.
 * When both Arduinos are ready, the Controller sends the starting game state, then sends the random seed to use
 */
class ClientNetworker : public GameNetworker {

public:
    ClientNetworker(LiquidCrystal* display, int ledCount, int* leds);

    void tryConnect() override;

protected:
    void checkConnection();

    //Override this methods, and they need extra code
    void setReady() override;
};


#endif //ARD_CLIENT_NETWORKER_H
