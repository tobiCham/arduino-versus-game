#ifndef ARD_NETWORKER_H
#define ARD_NETWORKER_H

#include <LiquidCrystal.h>
#include <HardwareSerial.h>

/**
 * Base class. Handles Networking functionality
 */
class GameNetworker {

public:

    /**
     * @param ledCount Number of LEDs which the player has
     * @param leds Array of the LEDs. These will turn on when the player is ready
     */
    GameNetworker(LiquidCrystal* display, int ledCount, int* leds);

    /**
     * Will attempt to connect with another Arduino. Will only return once the connection is completee
     */
    virtual void tryConnect() = 0;

    /**
     * @return The seed for the game
     */
    inline int getSeed() { return seed; };

    /**
     * Called when the button is pressed
     */
    virtual void readyButtonPress();

    /**
     * @return Whether it has been detected that the other player has lost the game
     */
    bool hasOtherPlayerLost();

    //Declare the data IDs which are used in the Protocol as static constants
    static const int BEGIN_CONNECTION = 1;
    static const int CLIENT_READY = 2;
    static const int STARTING = 3;
    static const int LOSE = 4;

protected:
    LiquidCrystal* const display; //LCD pointer to display info
    const int ledCount; //Number of LEDs to turn on
    int* const leds; //Array of LEDs

    int seed; //Random seed. This will be set by the sub class
    bool connected;
    bool ready;
    volatile bool buttonUpdate;

    //These three functions are used by both sub networkers, so declaring them in the base class
    //to add common functionality

    /**
     * Updates the LCD that the Arduinos are connected and the button can be pressed
     */
    virtual void setWaitReady();

    /**
     * Called after the button is pressed. Updates the LCD
     */
    virtual void setReady();
};


#endif //ARD_NETWORKER_H
