#ifndef ARD_PROJECTILE_H
#define ARD_PROJECTILE_H

#include "GameObject.h"

class Projectile : public GameObject {

public:
    Projectile(Game* game);

    void update() override;
    void render(LCDRenderer* renderer) override;
    void onIntersect(GameObject* otherObj) override;

    inline GameObjectType getType() override { return GameObjectType::PROJECTILE; }

    /**
     * Fires this projectile from the player, making it valid
     */
    void fire();

    /**
     * @return Whether the projectile is valid to render and update.
     */
    bool isValid();

private:
    bool hasHit; //Whether the projectile has hit a block (aka is valid)
    int speed; //Pixels to move per update
};


#endif //ARD_PROJECTILE_H
