#ifndef ARD_BLOCK_H
#define ARD_BLOCK_H

#include "LCDRenderer.h"
#include "GameObject.h"

/**
 * Block class. Each block is an obstacle the player must dodge or shoot.
 *
 * There are only a set a number of Blocks in the game, and there are only a set number of instances of Block.
 * Whether a block is displayed / updated and so considered part of the game is controlled by a boolean flag.
 * This is to aid with simple memory management
 */
class Block : public GameObject {

public:
    Block(Game* game);

    inline GameObjectType getType() override { return GameObjectType::BLOCK; }

    void render(LCDRenderer* renderer);
    void update();

    /**
     * Initialises the Block to be considered part of the game. Will spawn on the screen
     * and fall down towards the player
     */
    void init(int x, int y, int width, int height);

    bool intersects(GameObject* otherObject) override;


    inline void setValid(bool valid) { this->valid = valid; }

    /**
     * @return Whether the block is valid, and on the screen
     */
    bool isValid();

    //Getter and setter methods for the speed
    static inline void setSpeed(float speed) { Block::speed = speed; }
    static inline float getSpeed() { return Block::speed; }

private:
    bool valid;
    float accurateY;

    //All blocks share the same speed, so making it static
    static float speed;
};

#endif //ARD_BLOCK_H